package com.digicore.dto;

import java.util.Objects;

public class AccountInfo {

    private String accountName;
    private String accountNumber;
    private String password;
    private double accountBalance;


    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public double getAccountBalance() {
        return accountBalance;
    }

    public void setAccountBalance(double accountBalance) {
        this.accountBalance = accountBalance;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AccountInfo that = (AccountInfo) o;
        return Objects.equals(accountName, that.accountName);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("AccountInfo{");
        sb.append("accountName='").append(accountName).append('\'');
        sb.append(", accountNumber='").append(accountNumber).append('\'');
        sb.append(", password='").append(password).append('\'');
        sb.append(", accountBalance=").append(accountBalance);
        sb.append('}');
        return sb.toString();
    }
}
