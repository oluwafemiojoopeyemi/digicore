package com.digicore.dto;

import com.digicore.enums.TransactionType;

import java.time.LocalDateTime;
import java.util.Objects;

public class Transaction {
    private LocalDateTime transactionDate;
    private TransactionType transactionType;
    private String narration;
    private Double amount;
    private Double accountBalance ;
    private String accountNumber;



    public LocalDateTime getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(LocalDateTime transactionDate) {
        this.transactionDate = transactionDate;
    }

    public TransactionType getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(TransactionType transactionType) {
        this.transactionType = transactionType;
    }

    public String getNarration() {
        return narration;
    }

    public void setNarration(String narration) {
        this.narration = narration;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Double getAccountBalance() {
        return accountBalance;
    }

    public void setAccountBalance(Double accountBalance) {
        this.accountBalance = accountBalance;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
}

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Transaction{");
        sb.append("transactionDate=").append(transactionDate);
        sb.append(", transactionType=").append(transactionType);
        sb.append(", narration='").append(narration).append('\'');
        sb.append(", amount=").append(amount);
        sb.append(", accountBalance=").append(accountBalance);
        sb.append(", accountNumber='").append(accountNumber).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
