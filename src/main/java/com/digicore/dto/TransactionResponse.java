package com.digicore.dto;

import com.digicore.enums.TransactionType;

import java.time.LocalDateTime;
import java.util.List;

public class TransactionResponse {
   private  List<TransactionDto> transactions;

    public List<TransactionDto> getTransactions() {
        return transactions;
    }

    public void setTransactions(List<TransactionDto> transactions) {
        this.transactions = transactions;
    }
}
