package com.digicore.dto;

public class WithdrawalRequest {

    private String accountNumber;
    private String accountPassword;
    private Double withdrawnAmount;

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public Double getWithdrawnAmount() {
        return withdrawnAmount;
    }

    public void setWithdrawnAmount(Double withdrawnAmount) {
        this.withdrawnAmount = withdrawnAmount;
    }

    public String getAccountPassword() {
        return accountPassword;
    }

    public void setAccountPassword(String accountPassword) {
        this.accountPassword = accountPassword;
    }
}
