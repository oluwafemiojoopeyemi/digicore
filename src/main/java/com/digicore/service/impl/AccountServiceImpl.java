package com.digicore.service.impl;

import com.digicore.dto.*;
import com.digicore.enums.TransactionType;
import com.digicore.security.JwtTokenUtil;
import com.digicore.service.AccountService;
import com.digicore.util.ResponseCode;
import com.digicore.util.ResponseMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.security.SecureRandom;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class AccountServiceImpl implements AccountService {

private static List<AccountInfo> accountInfos= new ArrayList<>();
    private static List<Transaction> transactions= new ArrayList<>();

private double minimumInitialDeposit = 500;
   private BCryptPasswordEncoder bCryptPasswordEncoder;
    @Autowired
    private AuthenticationManager authenticationManager; @Autowired
    private JwtTokenUtil jwtTokenUtil;



@Override
public ResponseEntity<AccountCreationResponse> createAccount (AccountCreationRequest accountCreationRequest) {

    AccountCreationResponse accountCreationResponse = new AccountCreationResponse();
    accountCreationResponse.setMessage(ResponseMessage.ERROR);
    accountCreationResponse.setSuccess(false);
    accountCreationResponse.setResponseCode(ResponseCode.ERROR);

    try {


        System.out.println("Account infos  >>>>>>>>>>>>>>>>>>>>    "+accountInfos);

    if (accountCreationRequest.getInitialDeposit() < minimumInitialDeposit) {

        accountCreationResponse.setMessage("Minimum of "+minimumInitialDeposit +" is required");
        return new ResponseEntity<>(
                accountCreationResponse, new HttpHeaders(), HttpStatus.NOT_ACCEPTABLE);

    }

    String accountNumber = generateAccountNumber();

    for (AccountInfo info : accountInfos) {

        if (info.getAccountNumber().equals(accountNumber)) {

            accountNumber = generateAccountNumber();
            break;
        }

    }

        AccountInfo accountInfo = new AccountInfo();
        accountInfo.setAccountName(accountCreationRequest.getAccountName());

        if (accountInfos.contains(accountInfo)) {
            accountCreationResponse.setMessage("Account name already exist");
            return new ResponseEntity<>(
                    accountCreationResponse, new HttpHeaders(), HttpStatus.NOT_ACCEPTABLE);

        }
        bCryptPasswordEncoder = encoder();
        accountInfo.setPassword(bCryptPasswordEncoder.encode(accountCreationRequest.getAccountPassword()));


        accountInfo.setAccountBalance(accountCreationRequest.getInitialDeposit());
        accountInfo.setAccountNumber(accountNumber);
        accountInfos.add(accountInfo);
        accountCreationResponse.setSuccess(true);
        accountCreationResponse.setResponseCode(ResponseCode.SUCCESS);
        accountCreationResponse.setMessage(ResponseMessage.ACCOUNT_CREATION_SUCCESS);
        return new ResponseEntity<>(
                accountCreationResponse, new HttpHeaders(), HttpStatus.ACCEPTED);



} catch (Exception ex) {

    ex.printStackTrace();

    return new ResponseEntity<AccountCreationResponse>(
            accountCreationResponse, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);
}


}

@Override
    public ResponseEntity<AccountInfoResponse> getAccountInfo (String accountNumber) {

        AccountInfoResponse accountInfoResponse = new AccountInfoResponse();
        accountInfoResponse.setMessage(ResponseMessage.ERROR);
        accountInfoResponse.setSuccess(false);
        accountInfoResponse.setResponseCode(ResponseCode.ERROR);

        try {




AccountInfo accountInfo = null;

            for (AccountInfo info : accountInfos) {

                if (info.getAccountNumber().equals(accountNumber)) {
                    accountInfo =info;
                    break;
                }

            }


                if (Objects.isNull(accountInfo)) {
                    accountInfoResponse.setMessage("Account number not found");
                    return new ResponseEntity<>(
                            accountInfoResponse, new HttpHeaders(), HttpStatus.BAD_REQUEST);

                }

                AccountInfoResponse.Account account = new AccountInfoResponse.Account();
                account.setAccountName(accountInfo.getAccountName());
                account.setAccountNumber(accountInfo.getAccountNumber());
                account.setBalance(accountInfo.getAccountBalance());
accountInfoResponse.setAccount(account);
accountInfoResponse.setResponseCode(ResponseCode.SUCCESS);
            accountInfoResponse.setSuccess(true);
            accountInfoResponse.setMessage(ResponseMessage.ACCOUNT_INFO_SUCCESS);

                return new ResponseEntity<>(
                        accountInfoResponse, new HttpHeaders(), HttpStatus.ACCEPTED);



        } catch (Exception ex) {

            ex.printStackTrace();

            return new ResponseEntity<>(
                    accountInfoResponse, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);
        }


    }


@Override
    public ResponseEntity<LoginResponse> login (LoginRequest loginRequest) {

        LoginResponse loginResponse = new LoginResponse();
        loginResponse.setSuccess(false);
        loginResponse.setAccessToken("");
        try {
//            final Authentication authentication = authenticationManager.authenticate(
//                    new UsernamePasswordAuthenticationToken(
//                            loginRequest.getAccountNumber(),
//                            loginRequest.getAccountPassword()
//                    )
//            );

//            System.out.println("authentication  >>>>>>>>>>>>>>>>>     " + authentication);
//            System.out.println("got here  >>>>>>>>>>>>>>>>>     " + authentication);
            AccountInfo accountInfo = null;
            for (AccountInfo info : accountInfos) {

                if (info.getAccountNumber().equals(loginRequest.getAccountNumber()))
                    accountInfo = info;
                break;
            }

            System.out.println("accountInfo   :     " + accountInfo);
            if (Objects.isNull(accountInfo) || !bCryptPasswordEncoder.matches(loginRequest.getAccountPassword(), accountInfo.getPassword())) {

                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(loginResponse);


            }

           // SecurityContextHolder.getContext().setAuthentication(authentication);

            final String token = jwtTokenUtil.generateToken(accountInfo);
            System.out.println("token  >>>>>>>>>>>>>>>>>     " + token);

            loginResponse.setAccessToken(token);
            loginResponse.setSuccess(true);


            return ResponseEntity.status(HttpStatus.ACCEPTED).body(loginResponse);

        } catch (Exception ex) {
            ex.printStackTrace();

            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(loginResponse);



        }


    }

    @Override
    public ResponseEntity<TransactionResponse> getStatement (String accountNumber) {

        TransactionResponse transactionResponse = new TransactionResponse();


        try {


List<TransactionDto> transactionList  =  new ArrayList<>();

            System.out.println("transactions  >>>>>>>>>>>>>>>>>.    "+transactions);

for(Transaction transaction : transactions) {
    if(transaction.getAccountNumber().equals(accountNumber)) {

        TransactionDto transactionDto = new TransactionDto();
        transactionDto.setAccountBalance(transaction.getAccountBalance());
        transactionDto.setAmount(transaction.getAmount());
        transactionDto.setNarration(transaction.getNarration());
        transactionDto.setTransactionDate(transaction.getTransactionDate());
        transactionDto.setTransactionType(transaction.getTransactionType().name());
        transactionList.add(transactionDto);

    }



}
transactionResponse.setTransactions(transactionList);
            return new ResponseEntity<>(
                    transactionResponse, new HttpHeaders(), HttpStatus.ACCEPTED);



        } catch (Exception ex) {

            ex.printStackTrace();

            return new ResponseEntity<>(
                    transactionResponse, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);
        }


    }

@Override
    public ResponseEntity<AccountCreationResponse> deposit (DepositRequest depositRequest) {

        AccountCreationResponse depositResponse = new AccountCreationResponse();
        depositResponse.setMessage(ResponseMessage.ERROR);
        depositResponse.setSuccess(false);
        depositResponse.setResponseCode(ResponseCode.ERROR);

        try {


            if(depositRequest.getAmount()<1 || depositRequest.getAmount() >1000000){
                depositResponse.setMessage("Deposit amount should be between 1 and 1,0000,000");
                return new ResponseEntity<>(
                        depositResponse, new HttpHeaders(), HttpStatus.BAD_REQUEST);


            }



            AccountInfo accountInfo = null;

            for (AccountInfo info : accountInfos) {

                if (info.getAccountNumber().equals(depositRequest.getAccountNumber())) {
                    accountInfo =info;
                    break;
                }

            }

            if(Objects.isNull(accountInfo)) {

                depositResponse.setMessage("Account number does not exist");
                return new ResponseEntity<>(
                        depositResponse, new HttpHeaders(), HttpStatus.BAD_REQUEST);

            }

            List<Transaction> transactionList  =  transactions.stream().filter(e->e.getAccountNumber().
                    equals(depositRequest.getAccountNumber())).collect(Collectors.toList());
            Collections.reverse(transactionList);

            Double balance = 0d;

            if(!transactionList.isEmpty()) {

                balance=transactionList.get(0).getAccountBalance();

            }


            balance=balance+depositRequest.getAmount();


            Transaction transaction = new Transaction();
            transaction.setAccountNumber(depositRequest.getAccountNumber());
            transaction.setAmount(depositRequest.getAmount());
            transaction.setTransactionDate(LocalDateTime.now());
            transaction.setTransactionType(TransactionType.DEPOSIT);
            transaction.setAccountBalance(balance);

            transactions.add(transaction);



            depositResponse.setResponseCode(ResponseCode.SUCCESS);
            depositResponse.setSuccess(true);
            depositResponse.setMessage(ResponseMessage.SUCCESS);

            return new ResponseEntity<>(
                    depositResponse, new HttpHeaders(), HttpStatus.ACCEPTED);



        } catch (Exception ex) {

            ex.printStackTrace();

            return new ResponseEntity<>(
                    depositResponse, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);
        }


    }

    @Override
    public ResponseEntity<AccountCreationResponse> withdraw(WithdrawalRequest withdrawalRequest) {

        AccountCreationResponse withdrawalResponse = new AccountCreationResponse();
        withdrawalResponse.setMessage(ResponseMessage.ERROR);
        withdrawalResponse.setSuccess(false);
        withdrawalResponse.setResponseCode(ResponseCode.ERROR);

        try {

            if(withdrawalRequest.getWithdrawnAmount()<1){
                withdrawalResponse.setMessage("Deposit amount should be between 1 and 1,0000,000");
                return new ResponseEntity<>(
                        withdrawalResponse, new HttpHeaders(), HttpStatus.BAD_REQUEST);


            }



            AccountInfo accountInfo = null;

            for (AccountInfo info : accountInfos) {

                if (info.getAccountNumber().equals(withdrawalRequest.getAccountNumber())) {
                    accountInfo =info;
                    break;
                }

            }

            if(Objects.isNull(accountInfo)) {

                withdrawalResponse.setMessage("Account number does not exist");
                return new ResponseEntity<>(
                        withdrawalResponse, new HttpHeaders(), HttpStatus.BAD_REQUEST);

            }

            System.out.println("transactions  >>>>>>>>>>>>>>>>>.    "+transactions);


            List<Transaction> transactionList  =  transactions.stream().filter(e->e.getAccountNumber().
                    equals(withdrawalRequest.getAccountNumber())).collect(Collectors.toList());
            Collections.reverse(transactionList);

            Double balance = 0d;

            if(!transactionList.isEmpty()) {

                balance=transactionList.get(0).getAccountBalance();

            }


            balance=balance-withdrawalRequest.getWithdrawnAmount();


            Transaction transaction = new Transaction();
            transaction.setAccountNumber(withdrawalRequest.getAccountNumber());
            transaction.setAmount(withdrawalRequest.getWithdrawnAmount());
            transaction.setTransactionDate(LocalDateTime.now());
            transaction.setTransactionType(TransactionType.WITHDRAWAL);
            transaction.setAccountBalance(balance);

            transactions.add(transaction);



            withdrawalResponse.setResponseCode(ResponseCode.SUCCESS);
            withdrawalResponse.setSuccess(true);
            withdrawalResponse.setMessage(ResponseMessage.SUCCESS);

            return new ResponseEntity<>(
                    withdrawalResponse, new HttpHeaders(), HttpStatus.ACCEPTED);



        } catch (Exception ex) {

            ex.printStackTrace();

            return new ResponseEntity<>(
                    withdrawalResponse, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);
        }


    }


    private String generateAccountNumber(){
    SecureRandom objGenerator = new SecureRandom();
    long random = (long)(objGenerator. nextDouble()*10000000000L);
    return String.valueOf(random);
}

    public BCryptPasswordEncoder encoder() {
        return new BCryptPasswordEncoder();
    }



}
