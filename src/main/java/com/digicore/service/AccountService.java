package com.digicore.service;


import com.digicore.dto.*;
import org.springframework.http.ResponseEntity;

public interface AccountService {

    public ResponseEntity<AccountCreationResponse> createAccount (AccountCreationRequest accountCreationRequest) ;
    public ResponseEntity<AccountInfoResponse> getAccountInfo (String accountNumber) ;
    public ResponseEntity<LoginResponse> login (LoginRequest loginRequest);
    public ResponseEntity<TransactionResponse> getStatement (String accountNumber) ;
    public ResponseEntity<AccountCreationResponse> deposit (DepositRequest depositRequest) ;
    public ResponseEntity<AccountCreationResponse> withdraw(WithdrawalRequest withdrawalRequest) ;


}
