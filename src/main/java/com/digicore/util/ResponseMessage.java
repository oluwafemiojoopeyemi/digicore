package com.digicore.util;

public interface ResponseMessage {

    public static final String ERROR ="Unknown Error Occured! Please try again later";
    public static final String SUCCESS ="Operation Successful";
    public static final String ACCOUNT_CREATION_SUCCESS ="Account created successfully";
    public static final String ACCOUNT_INFO_SUCCESS ="Account fetched successfully";


}
