package com.digicore.controller;

import com.digicore.dto.*;
import com.digicore.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.concurrent.Future;

@RestController
@RequestMapping("/app")
public class AccountController {

    @Autowired
    private AccountService accountService;

    @Async
    @PostMapping(value ="create_account" ,consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public Future<ResponseEntity<AccountCreationResponse>> createAccount ( @RequestBody AccountCreationRequest accountCreationRequest) {
        return new AsyncResult<>( accountService.createAccount(accountCreationRequest));
    }

    @Async
    @GetMapping(value ="account_info" , produces = MediaType.APPLICATION_JSON_VALUE)
    public Future<ResponseEntity<AccountInfoResponse>> getAccountInfo (@RequestParam String accountNumber) {
        return new AsyncResult<>( accountService.getAccountInfo(accountNumber));
    }

    @Async
    @PostMapping(value ="login" ,consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public Future<ResponseEntity<LoginResponse>> login ( @RequestBody LoginRequest loginRequest) {
        return new AsyncResult<>( accountService.login(loginRequest));
    }


    @Async
    @GetMapping( value ="account_statement" ,produces = MediaType.APPLICATION_JSON_VALUE)
    public Future<ResponseEntity<TransactionResponse>> getStatement (@RequestParam String accountNumber) {
        return new AsyncResult<>( accountService.getStatement(accountNumber));
    }

    @Async
    @PostMapping(value ="deposit" ,consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public Future<ResponseEntity<AccountCreationResponse>> deposit ( @RequestBody DepositRequest depositRequest ) {
        return new AsyncResult<>( accountService.deposit(depositRequest));
    }

    @Async
    @PostMapping(value ="withdrawal" ,consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public Future<ResponseEntity<AccountCreationResponse>> withdraw( @RequestBody WithdrawalRequest withdrawalRequest ) {
        return new AsyncResult<>( accountService.withdraw(withdrawalRequest));
    }

}
